package com.tecnostore.controllers;
import com.tecnostore.exceptions.PedidosNotFoundException;
import com.tecnostore.models.Pedidos;
import com.tecnostore.repositories.PedidosRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PedidosController {
    private final PedidosRepository pedidosRepository;

    public PedidosController (PedidosRepository pedidosRepository) {this.pedidosRepository = pedidosRepository;}

    @GetMapping("/pedidos")
    List<Pedidos> getAllPedidos(){return this.pedidosRepository.findAll();}

    @GetMapping("/pedidos/{Id}")
    Pedidos getPedidos(@PathVariable String Id){
        return pedidosRepository.findById(Id)
                .orElseThrow(() -> new PedidosNotFoundException("No se encontro una orden con: " + Id));
    }

    @DeleteMapping("/pedidos/{Id}")
    void deletePedidos(@PathVariable String Id){this.pedidosRepository.deleteById(Id);
    }


    @PostMapping("/pedidos")
    Pedidos createPedidos(@RequestBody Pedidos pedidos){
        return this.pedidosRepository.save(pedidos);
    }
}
