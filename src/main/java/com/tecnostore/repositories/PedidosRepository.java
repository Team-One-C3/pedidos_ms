package com.tecnostore.repositories;

import com.tecnostore.models.Pedidos;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PedidosRepository extends MongoRepository<Pedidos,String> {

}
