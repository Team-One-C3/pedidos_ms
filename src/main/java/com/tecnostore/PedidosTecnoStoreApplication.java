package com.tecnostore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PedidosTecnoStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(PedidosTecnoStoreApplication.class, args);
    }

}
