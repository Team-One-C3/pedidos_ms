package com.tecnostore.models;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Pedidos {
    @Id
    private String numeroOrden;
    private String tipoProducto;
    private String username;
    private String ciudad;
    private Integer cantidad;
    private Integer precio;
    private String descripcion;
    private String celular;
    private String direccion;
    private Date fecha;

    public Pedidos(String numeroOrden, String tipoProducto, String username, String ciudad, Integer cantidad, Integer precio, String descripcion, String celular, String direccion, Date fecha) {
        this.numeroOrden = numeroOrden;
        this.tipoProducto = tipoProducto;
        this.username = username;
        this.ciudad = ciudad;
        this.cantidad = cantidad;
        this.precio = precio;
        this.descripcion = descripcion;
        this.celular = celular;
        this.direccion = direccion;
        this.fecha = fecha;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
