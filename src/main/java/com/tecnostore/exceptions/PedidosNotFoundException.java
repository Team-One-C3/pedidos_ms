package com.tecnostore.exceptions;

public class PedidosNotFoundException extends RuntimeException {
    public PedidosNotFoundException(String message) {
        super(message);
    }
}
